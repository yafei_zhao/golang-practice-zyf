# Go language example programming

## How to
1. set up GOPATH in your system variables, make sure this project is under $(GOPATH)/src;
2. cd $(GOPATH)/src
3. run or install:
    1. only run: 
    ```cmd
    go run golang-ex/hello
    ```

    2. install and run: 
    ```cmd
    go install golang-ex/stringutil
    go install golang-ex/hello
    ../bin/hello
    ```

